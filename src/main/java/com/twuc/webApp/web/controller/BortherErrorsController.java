package com.twuc.webApp.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tao.dong
 */
@RestController
@RequestMapping("/api")
public class BortherErrorsController {

    @GetMapping("/brother-errors/illegal-argument")
    public void get() {
        throw new IllegalArgumentException();
    }
}
