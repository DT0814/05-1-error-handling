package com.twuc.webApp.web.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author tao.dong
 */
@ControllerAdvice
public class AdvicerController extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value
            = {IllegalArgumentException.class})
    protected ResponseEntity<String> handleConflict() {
        return ResponseEntity
                .status(HttpStatus.EXPECTATION_FAILED)
                .contentType(MediaType.APPLICATION_JSON)
                .body("Something wrong with brother or sister.");
    }


}
