package com.twuc.webApp.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

/**
 * @author tao.dong
 */
@RestController
@RequestMapping("/api")
public class RuntimeExceptionController {
    @GetMapping("/exceptions/runtime-exception")
    public void throwRuntimeException() {
        throw new RuntimeException("throwRuntimeException");
    }

    @GetMapping("/exceptions/access-exception")
    public void throwAccessException() {
        throw new AccessControlException("throwAccessException");
    }


    @ExceptionHandler()
    public ResponseEntity<String> CatchException(RuntimeException e){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("handler "+e.getMessage());
    }

    @ExceptionHandler()
    public ResponseEntity<String> CatchAccessControllerCException(AccessControlException e){
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("real handler "+e.getMessage());
    }
}
