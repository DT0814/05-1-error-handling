package com.twuc.webApp.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController
@RequestMapping("/api/exceptions")
public class ErrorController {
    @GetMapping("/null-pointer")
    public void throwNullPointerException() {
        throw new NullPointerException();
    }

    @GetMapping("/arithmetic")
    public void throwArithmeticException() {
        throw new ArithmeticException();
    }
    @ExceptionHandler({NullPointerException.class,ArithmeticException.class})
    public ResponseEntity<String> CatchException(){
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body("Something wrong with the argument");
    }

}
