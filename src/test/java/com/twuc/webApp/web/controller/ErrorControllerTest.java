package com.twuc.webApp.web.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class ErrorControllerTest {
    @Autowired
    private TestRestTemplate template;

    @Test
    void test_null_pointer() {
        ResponseEntity<String> forEntity = template.getForEntity("/api/exceptions/null-pointer", String.class);
        Assertions.assertEquals(HttpStatus.I_AM_A_TEAPOT, forEntity.getStatusCode());
        Assertions.assertEquals(forEntity.getBody(), "Something wrong with the argument");
    }

    @Test
    void test_arithmetic() {
        ResponseEntity<String> forEntity = template.getForEntity("/api/exceptions/arithmetic", String.class);
        Assertions.assertEquals(HttpStatus.I_AM_A_TEAPOT, forEntity.getStatusCode());
        Assertions.assertEquals(forEntity.getBody(), "Something wrong with the argument");
    }
}
