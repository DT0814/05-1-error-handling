package com.twuc.webApp.web.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class RuntimeExceptionControllerTest {

    @Autowired
    TestRestTemplate template;

    @Test
    void catch_exception() {
        ResponseEntity<String> forEntity = template.getForEntity("/api/exceptions/runtime-exception", String.class);
        Assertions.assertEquals(forEntity.getStatusCode(), HttpStatus.NOT_FOUND);
        Assertions.assertEquals(forEntity.getBody(),"handler throwRuntimeException");
    }

    @Test
    void catch_exception_access() {
        ResponseEntity<String> forEntity = template.getForEntity("/api/exceptions/access-exception", String.class);
        Assertions.assertEquals(forEntity.getStatusCode(), HttpStatus.FORBIDDEN);
        Assertions.assertEquals(forEntity.getBody(),"real handler throwAccessException");
    }
}
